// 床井さんのところからぱくり
// http://marina.sys.wakayama-u.ac.jp/~tokoi/?date=20040321

// クォータニオンはこの辺参照
// https://wgld.org/d/webgl/w031.html
// http://marupeke296.com/DXG_No10_Quaternion.html

#pragma once
#include <math.h>

class TTrackball{
public:
	// コンストラクタ
	TTrackball(){
		// クォータニオンと行列初期化
		cq[0] = tq[0] = 1.0;
		cq[1] = tq[1] = 0.0;
		cq[2] = tq[2] = 0.0;
		cq[3] = tq[3] = 0.0;

		qrot(rt, cq);
	}
	~TTrackball(){}

private:
	// 回転の初期値とドラッグ中の回転 (クォータニオン)
	double cq[4], tq[4];

	// 回転の変換行列
	double rt[16];

	// ドラッグ開始位置
	int cx, cy;

	// マウスの絶対位置→ウィンドウ内での相対位置の換算係数
	double sx, sy;

	// マウスの相対位置→回転角の換算係数
	const double scale = (2.0 * 3.14159265358979323846);

	// クリックのフラグ
	bool clickFlag = false;

	// クォータニオンの積 r <- p x q
	void qmul(double r[], const double p[], const double q[])
	{
		r[0] = p[0] * q[0] - p[1] * q[1] - p[2] * q[2] - p[3] * q[3];
		r[1] = p[0] * q[1] + p[1] * q[0] + p[2] * q[3] - p[3] * q[2];
		r[2] = p[0] * q[2] - p[1] * q[3] + p[2] * q[0] + p[3] * q[1];
		r[3] = p[0] * q[3] + p[1] * q[2] - p[2] * q[1] + p[3] * q[0];
	}

	// 回転の変換行列 r <- クォータニオン q
	void qrot(double r[], double q[])
	{
		double x2 = q[1] * q[1] * 2.0;
		double y2 = q[2] * q[2] * 2.0;
		double z2 = q[3] * q[3] * 2.0;
		double xy = q[1] * q[2] * 2.0;
		double yz = q[2] * q[3] * 2.0;
		double zx = q[3] * q[1] * 2.0;
		double xw = q[1] * q[0] * 2.0;
		double yw = q[2] * q[0] * 2.0;
		double zw = q[3] * q[0] * 2.0;

		r[0] = 1.0 - y2 - z2;
		r[1] = xy + zw;
		r[2] = zx - yw;
		r[4] = xy - zw;
		r[5] = 1.0 - z2 - x2;
		r[6] = yz + xw;
		r[8] = zx + yw;
		r[9] = yz - xw;
		r[10] = 1.0 - x2 - y2;
		r[3] = r[7] = r[11] = r[12] = r[13] = r[14] = 0.0;
		r[15] = 1.0;
	}


public:
	// ドラッグ開始位置の保存
	void Click(int _x, int _y){ cx = _x; cy = _y; clickFlag = true; }

	// ドラッグ終了
	void Up(void){ for (int i = 0; i < 4; i++) cq[i] = tq[i]; clickFlag = false; }

	// 画面の大きさ変わったときの関数
	void Resize(int _w, int _h){ sx = 1.0 / (double)_w; sy = 1.0 / (double)_h; }

	// ドラッグ中
	void Drag(int _x, int _y)
	{
		if (!clickFlag) return;

		// マウスポインタの位置のドラッグ開始位置からの変位 (相対値)(0~1)
		double dx = (_x - cx) * sx;
		double dy = (_y - cy) * sy;

		// マウスポインタの位置のドラッグ開始位置からの距離 (相対値)(0~1)
		double a = sqrt(dx * dx + dy * dy);

		if (a != 0.0) {
			// マウスのドラッグに伴う回転のクォータニオン dq を求める
			double ar = a * scale * 0.5;
			double as = sin(ar) / a;
			double dq[4] = { cos(ar), dy*as, dx*as, 0 };

			// 回転の初期値 cq に dq を掛けて回転を合成する
			qmul(tq, dq, cq);

			// クォータニオンから回転の変換行列を求める
			qrot(rt, tq);
		}
	}

	// 元に戻す
	void uni(){ cq[0] = 1.0; cq[1] = 0.0; cq[2] = 0.0; cq[3] = 0.0; qrot(rt, cq); }

	// 三次元座標をもらって回転させる
	void RotateVec(double x[]){
		double Rq[4] = { tq[0], -tq[1], -tq[2], -tq[3] };
		double Pq[4] = { 0, x[0], x[1], x[2] };
		double resQ[4];

		qmul(resQ, Rq, Pq);
		qmul(Pq, resQ, tq);

		x[0] = Pq[1];
		x[1] = Pq[2];
		x[2] = Pq[3];		
	}

	// 回転行列を返す
	double *GetMatrix(){
		return rt;
	}

	// flagを返す
	bool GetClickFlag(){
		return clickFlag;
	}

};