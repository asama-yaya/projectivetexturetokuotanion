#version 330
// shadertype=<glsl>
// vert

// 渡す変数
out vec4 position;
out vec3 normal;
out vec4 texcoord;

out vec4 projectiveTex;

// 受け取る行列
uniform mat4 matPC;
uniform mat4 matM;
uniform mat3 matNormal;

uniform mat4 mTPV;

// おまじないに近いもの
layout (location = 0) in vec4 pv; 
layout (location = 2) in vec3 nv; 
layout (location = 8) in vec4 tv;

void main(void){

	position = matM * pv;
	normal = normalize(matNormal * nv);
	texcoord = tv;

	projectiveTex = mTPV * position;

	gl_Position = matPC * position;
}