#include "main.h"


int main(int argc, char *argv[]){

	// glutのコールバック関数
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
	glutInitWindowPosition(0, 0);
	glutInitWindowSize(window_width, window_height);
	glutCreateWindow("MainWindow");
	glutDisplayFunc(display);
	glutReshapeFunc(resize);
	glutMouseFunc(mouse);
	glutMotionFunc(motion);
	glutKeyboardFunc(keyboard);
	glutIdleFunc(idle);

	// 初期化
	glClearColor(0.0f, 0.3f, 0.0f, 0.0f);
	glEnable(GL_DEPTH_TEST);

	glDisable(GL_CULL_FACE);					// 両面

	// shader init
	InitShader();

	// obj load
	if (!Objmesh.LoadFile("data\\texture.obj")){
		getchar();
		exit(0);
	}

	// main loop
	glutMainLoop();
	return 0;
}

// display
void display(void){

	// GPUに送る行列用
	GLfloat gl_pc[16], gl_m[16], gl_normal[9];


	// シェーダプログラムの適用 
	glUseProgram(glProgram);

	// textureの有効化
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, testTexId);

	// 画面クリア
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// viewport
	glViewport(0, 0, window_width, window_height);

	// 透視投影うんぬん
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(60, (double)window_width / window_height, 10, 1000.0);
	gluLookAt(cameraPos[0], cameraPos[1], cameraPos[2], 0, 0, 0, cameraUp[0], cameraUp[1], cameraUp[2]);

	// projection matrixをぽーい
	glGetFloatv(GL_PROJECTION_MATRIX, gl_pc);
	glUniformMatrix4fv(p_testMatPC, 1, GL_FALSE, gl_pc);

	// モデルビュー変換行列の初期化
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glMultMatrixd(Trackball.GetMatrix());

	// modelview matrixをぽーい
	glGetFloatv(GL_MODELVIEW_MATRIX, gl_m);
	glUniformMatrix4fv(p_testMatM, 1, GL_FALSE, gl_m);
	gl_normal[0] = gl_m[0]; gl_normal[1] = gl_m[1]; gl_normal[2] = gl_m[2];
	gl_normal[3] = gl_m[4]; gl_normal[4] = gl_m[5]; gl_normal[5] = gl_m[6];
	gl_normal[6] = gl_m[8]; gl_normal[7] = gl_m[9]; gl_normal[8] = gl_m[10];
	glUniformMatrix3fv(p_testMatNormal, 1, GL_FALSE, gl_normal);

	// 描画
	Objmesh.Draw();

	// 後ろの板を描画
	DrawPlanes();

	// テクスチャのバインド解除
	glBindTexture(GL_TEXTURE_2D, 0);

	// シェーダプログラムの使用終了
	glUseProgram(0);

	// プロジェクタがある位置に球体を描画
	glLoadIdentity();
	glTranslatef(proPosition[0], proPosition[1], proPosition[2]);
	glutSolidSphere(10.0, 10, 10);

	// ダブルバッファリング
	glutSwapBuffers();

	// 画像保存
	if (capture_flag){
		cv::Mat t_img(window_height, window_width, CV_8UC3);
		glReadPixels(0, 0, t_img.cols, t_img.rows, GL_BGR, GL_UNSIGNED_BYTE, t_img.data);
		cv::flip(t_img, t_img, 0);
		cv::imwrite("data\\resultimg.png", t_img);
		capture_flag = false;
		cout << "capture !" << endl;
	}


}

// shader
void InitShader(){

	// glew初期化
	glewInit();

	// シェーダ読み込み
	MakeShader(glProgram, "test.vert", "test.frag");


	// 射影テクスチャの初期化
	Projective_init();

	// 場所とる
	glUseProgram(glProgram);
	p_testMatM = glGetUniformLocation(glProgram, "matM");
	p_testMatNormal = glGetUniformLocation(glProgram, "matNormal");
	p_testMatPC = glGetUniformLocation(glProgram, "matPC");

	// テクスチャ(投影するもの)
	glActiveTexture(GL_TEXTURE1);
	LoadGLTexture("data\\a.png", testTexId, false);

	// シェーダプログラムへ適用
	glUniform1i(glGetUniformLocation(glProgram, "tex1"), 1);

	// 解除
	glUseProgram(0);
}

// 投影テクスチャマッピングの初期化(行列をシェーダに送る)
void Projective_init(void){
	// なんでか知らんが、途中までGL行列で計算しとるわ
	cv::Mat mT, mV, mP, mTPV;

	// 射影からテクスチャへの変換
	cv::Mat tnkmat = (cv::Mat_<double>(4, 4) <<
		0.5, 0.0, 0.0, 0.0,
		0.0, 0.5, 0.0, 0.0,
		0.0, 0.0, 1.0, 0.0,
		0.5, 0.5, 0.0, 1.0);
	mT = tnkmat.clone();

	// ビュー行列
	double tntn;
	cv::Point3d lightPosition = cv::Point3d(proPosition[0], proPosition[1], proPosition[2]);
	cv::Point3d lightUp = cv::Point3d(proUp[0], proUp[1], proUp[2]);
	cv::Point3d lightTarget = cv::Point3d(proTarget[0], proTarget[1], proTarget[2]);
	cv::Point3d zaxis = lightPosition - lightTarget;
	tntn = cv::norm(zaxis); zaxis.x /= tntn; zaxis.y /= tntn; zaxis.z /= tntn;
	cv::Point3d xaxis = lightUp.cross(zaxis);
	tntn = cv::norm(xaxis); xaxis.x /= tntn; xaxis.y /= tntn; xaxis.z /= tntn;
	cv::Point3d yaxis = zaxis.cross(xaxis);
	tntn = cv::norm(yaxis); yaxis.x /= tntn; yaxis.y /= tntn; yaxis.z /= tntn;
	mV = cv::Mat::eye(4, 4, CV_64F);
	mV.at<double>(0, 0) = xaxis.x; mV.at<double>(1, 0) = xaxis.y; mV.at<double>(2, 0) = xaxis.z;
	mV.at<double>(0, 1) = yaxis.x; mV.at<double>(1, 1) = yaxis.y; mV.at<double>(2, 1) = yaxis.z;
	mV.at<double>(0, 2) = zaxis.x; mV.at<double>(1, 2) = zaxis.y; mV.at<double>(2, 2) = zaxis.z;
	mV.at<double>(3, 0) = -xaxis.dot(lightPosition);
	mV.at<double>(3, 1) = -yaxis.dot(lightPosition);
	mV.at<double>(3, 2) = -zaxis.dot(lightPosition);

	// 射影行列
	if (false){
		double p[3][3] = { 500.0, 0.0, window_width / 2.0, 0.0, 500.0, window_height / 2.0, 0.0, 0.0, 1.0 };
		double pfar = 10000.0, pnear = 0.1;
		mP = cv::Mat::zeros(4, 4, CV_64F);
		mP.at<double>(0, 0) = 2.0 * p[0][0] / (double)(window_width - 1.0);
		mP.at<double>(1, 1) = -2.0 * p[1][1] / (double)(window_height - 1.0);
		mP.at<double>(2, 0) = -(2.0 * p[0][2] / (double)(window_width - 1.0) - 1.0);
		mP.at<double>(2, 1) = -(2.0 * p[1][2] / (double)(window_height - 1.0) - 1.0);
		mP.at<double>(2, 2) = (pfar + pnear) / (pnear - pfar);
		mP.at<double>(2, 3) = -1.0;
		mP.at<double>(3, 2) = 2.0*pfar*pnear / (pnear - pfar);
	}
	else{
		double fieldof = 30.0, aspectratio = 1.0, pnear = 100.0, pfar = 1000.0;
		fieldof = fieldof / 180.0 * 3.14;
		double yScale = 1.0 / tan(fieldof / 2.0);
		double xScale = yScale / aspectratio;
		mP = cv::Mat::zeros(4, 4, CV_64F);
		mP.at<double>(0, 0) = xScale;
		mP.at<double>(1, 1) = yScale;
		mP.at<double>(2, 2) = (pnear + pfar) / (pnear - pfar);
		mP.at<double>(2, 3) = -1.0;
		mP.at<double>(3, 2) = 2.0 * (pnear*pfar) / (pnear - pfar);
	}

	// mVPT
	mT = mT.t(); mP = mP.t(); mV = mV.t();
	mTPV = mT*mP*mV;

	// 配列に格納
	GLfloat ptnk[16];
	for (int ii = 0; ii < 4; ii++)
	for (int jj = 0; jj < 4; jj++){
		ptnk[ii * 4 + jj] = mTPV.at<double>(jj, ii);
	}

	// シェーダにぽいぽいぽーい
	glUseProgram(glProgram);
	glUniformMatrix4fv(glGetUniformLocation(glProgram, "mTPV"), 1, GL_FALSE, ptnk);
	glUseProgram(0);
}

// 板の描画
void DrawPlanes(){

	// GPUに送る行列用
	GLfloat gl_m[16], gl_normal[9];

	glLoadIdentity();
	glTranslated(0, 0, -100);
	glScaled(200.0, 200.0, 1.0);
	glGetFloatv(GL_MODELVIEW_MATRIX, gl_m);
	glUniformMatrix4fv(p_testMatM, 1, GL_FALSE, gl_m);
	gl_normal[0] = gl_m[0]; gl_normal[1] = gl_m[1]; gl_normal[2] = gl_m[2];
	gl_normal[3] = gl_m[4]; gl_normal[4] = gl_m[5]; gl_normal[5] = gl_m[6];
	gl_normal[6] = gl_m[8]; gl_normal[7] = gl_m[9]; gl_normal[8] = gl_m[10];
	glUniformMatrix3fv(p_testMatNormal, 1, GL_FALSE, gl_normal);
	glutSolidCube(1.0);

	glLoadIdentity();
	glTranslated(0, -100, 0);
	glScaled(200.0, 1.0, 200.0);
	glGetFloatv(GL_MODELVIEW_MATRIX, gl_m);
	glUniformMatrix4fv(p_testMatM, 1, GL_FALSE, gl_m);
	gl_normal[0] = gl_m[0]; gl_normal[1] = gl_m[1]; gl_normal[2] = gl_m[2];
	gl_normal[3] = gl_m[4]; gl_normal[4] = gl_m[5]; gl_normal[5] = gl_m[6];
	gl_normal[6] = gl_m[8]; gl_normal[7] = gl_m[9]; gl_normal[8] = gl_m[10];
	glUniformMatrix3fv(p_testMatNormal, 1, GL_FALSE, gl_normal);
	glutSolidCube(1.0);

	glLoadIdentity();
	glTranslated(-100, 0, 0);
	glScaled(1.0, 200.0, 200.0);
	glGetFloatv(GL_MODELVIEW_MATRIX, gl_m);
	glUniformMatrix4fv(p_testMatM, 1, GL_FALSE, gl_m);
	gl_normal[0] = gl_m[0]; gl_normal[1] = gl_m[1]; gl_normal[2] = gl_m[2];
	gl_normal[3] = gl_m[4]; gl_normal[4] = gl_m[5]; gl_normal[5] = gl_m[6];
	gl_normal[6] = gl_m[8]; gl_normal[7] = gl_m[9]; gl_normal[8] = gl_m[10];
	glUniformMatrix3fv(p_testMatNormal, 1, GL_FALSE, gl_normal);
	glutSolidCube(1.0);
}

//********************************************************
// シェーダー初期化関連の関数
//********************************************************
// テクスチャの読み込み
void LoadGLTexture(string _fileName, GLuint &_texID, const bool _rectFlag){

	// 読み込み
	cv::Mat image = cv::imread(_fileName, -1);
	cv::flip(image, image, 0);
	if (image.channels() == 3){
		cv::cvtColor(image, image, CV_BGR2BGRA);
	}
	else if (image.channels() == 1){
		cv::cvtColor(image, image, CV_GRAY2BGRA);
	}

	// rect or?
	unsigned int texVersion;
	if (_rectFlag) texVersion = GL_TEXTURE_RECTANGLE;
	else texVersion = GL_TEXTURE_2D;

	if (_texID == -1){
		glGenTextures(1, &_texID);
		glBindTexture(texVersion, _texID);

		// テクスチャ画像はバイト単位に詰め込まれている 
		glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
		//glTexParameteri(texVersion, GL_GENERATE_MIPMAP, GL_TRUE);

		// テクスチャの割り当て 
		glTexImage2D(texVersion, 0, GL_RGBA, image.cols, image.rows, 0, GL_BGRA, GL_UNSIGNED_BYTE, image.data);

		// テクスチャを拡大・縮小する方法の指定 
		glTexParameteri(texVersion, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(texVersion, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		//glTexParameteri(texVersion, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		//glTexParameteri(texVersion, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

		// テクスチャの繰り返し方法の指定 
		//glTexParameteri(texVersion, GL_TEXTURE_WRAP_S, GL_CLAMP);
		//glTexParameteri(texVersion, GL_TEXTURE_WRAP_T, GL_CLAMP);
		glTexParameteri(texVersion, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
		glTexParameteri(texVersion, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
		GLfloat an[3] = { 1.0, 1.0, 1.0 };
		glTexParameterfv(texVersion, GL_TEXTURE_BORDER_COLOR, an);

		// マテリアルの情報を使うかどうかの指定
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
	}
	else{
		// bind
		glBindTexture(texVersion, _texID);

		// テクスチャの割り当て 
		glTexImage2D(texVersion, 0, GL_RGBA, image.cols, image.rows, 0, GL_BGRA, GL_UNSIGNED_BYTE, image.data);
	}

	// テクスチャのバインド解除
	glBindTexture(texVersion, 0);
}

// シェーダープログラムの初期化
void MakeShader(GLuint &_gl2Program, string _vertName, string _fragName)
{
	cout << "shader compile (" << _vertName << ", " << _fragName << ") -> ";

	// シェーダプログラムのコンパイル／リンク結果を得る変数 
	GLint compiled, linked;

	GLuint vertShader;
	GLuint fragShader;

	// シェーダオブジェクトの作成 
	vertShader = glCreateShader(GL_VERTEX_SHADER);
	fragShader = glCreateShader(GL_FRAGMENT_SHADER);

	// シェーダのソースプログラムの読み込み 
	if (ReadShaderSource(vertShader, _vertName.c_str())) { getchar(); exit(1); }
	if (ReadShaderSource(fragShader, _fragName.c_str())) { getchar(); exit(1); }

	// バーテックスシェーダのソースプログラムのコンパイル 
	glCompileShader(vertShader);
	glGetShaderiv(vertShader, GL_COMPILE_STATUS, &compiled);
	PrintShaderInfoLog(vertShader);
	if (compiled == GL_FALSE) {
		fprintf(stderr, "Compile error in vertex shader.\n");
		getchar();
		exit(1);
	}

	// フラグメントシェーダのソースプログラムのコンパイル 
	glCompileShader(fragShader);
	glGetShaderiv(fragShader, GL_COMPILE_STATUS, &compiled);
	PrintShaderInfoLog(fragShader);
	if (compiled == GL_FALSE) {
		fprintf(stderr, "Compile error in fragment shader.\n");
		getchar();
		exit(1);
	}

	// プログラムオブジェクトの作成 
	_gl2Program = glCreateProgram();

	// シェーダオブジェクトのシェーダプログラムへの登録 
	glAttachShader(_gl2Program, vertShader);
	glAttachShader(_gl2Program, fragShader);

	// シェーダオブジェクトの削除 
	glDeleteShader(vertShader);
	glDeleteShader(fragShader);

	// シェーダプログラムのリンク 
	glLinkProgram(_gl2Program);
	glGetProgramiv(_gl2Program, GL_LINK_STATUS, &linked);
	PrintProgramInfoLog(_gl2Program);
	if (linked == GL_FALSE) {
		fprintf(stderr, "Link error.\n");
		getchar();
		exit(1);
	}
	cout << "おしり" << endl;
}

// シェーダーのソースプログラムをメモリに読み込む
int ReadShaderSource(GLuint _shader, const char *_file){
	FILE *fp;
	const GLchar *source;
	GLsizei length;
	int ret;

	// ファイルを開く 
	fopen_s(&fp, _file, "rb");
	if (fp == NULL) {
		perror(_file);
		return -1;
	}

	// ファイルの末尾に移動し現在位置（つまりファイルサイズ）を得る 
	fseek(fp, 0L, SEEK_END);
	length = ftell(fp);

	// ファイルサイズのメモリを確保 
	source = (GLchar *)malloc(length);
	if (source == NULL) {
		fprintf(stderr, "Could not allocate read buffer.\n");
		return -1;
	}

	// ファイルを先頭から読み込む 
	fseek(fp, 0L, SEEK_SET);
	ret = fread((void *)source, 1, length, fp) != (size_t)length;
	fclose(fp);

	// シェーダのソースプログラムのシェーダオブジェクトへの読み込み 
	if (ret)
		fprintf(stderr, "Could not read file: %s.\n", _file);
	else
		glShaderSource(_shader, 1, &source, &length);

	// 確保したメモリの開放 
	free((void *)source);

	return ret;
}

// シェーダの情報を表示する
void PrintShaderInfoLog(GLuint _shader){
	GLsizei bufSize;

	glGetShaderiv(_shader, GL_INFO_LOG_LENGTH, &bufSize);

	if (bufSize > 1) {
		GLchar *infoLog;

		infoLog = (GLchar *)malloc(bufSize);
		if (infoLog != NULL) {
			GLsizei length;

			glGetShaderInfoLog(_shader, bufSize, &length, infoLog);
			fprintf(stderr, "InfoLog:\n%s\n\n", infoLog);
			free(infoLog);
		}
		else
			fprintf(stderr, "Could not allocate InfoLog buffer.\n");
	}
}

// プログラムの情報を表示する
void PrintProgramInfoLog(GLuint _program){
	GLsizei bufSize;

	glGetProgramiv(_program, GL_INFO_LOG_LENGTH, &bufSize);

	if (bufSize > 1) {
		GLchar *infoLog;

		infoLog = (GLchar *)malloc(bufSize);
		if (infoLog != NULL) {
			GLsizei length;

			glGetProgramInfoLog(_program, bufSize, &length, infoLog);
			fprintf(stderr, "InfoLog:\n%s\n\n", infoLog);
			free(infoLog);
		}
		else
			fprintf(stderr, "Could not allocate InfoLog buffer.\n");
	}
}


//********************************************************
// glutのコールバック関数
//********************************************************
void resize(int _w, int _h)
{
	// トラックボールする範囲
	Trackball.Resize(_w, _h);
	cameraTrackball.Resize(_w, _h);

	//// ウィンドウ全体をビューポートにする
	//glViewport(0, 0, _w, _h);

	//// 透視変換行列の指定
	//glMatrixMode(GL_PROJECTION);

	//// 透視変換行列の初期化 
	//glLoadIdentity();
	//gluPerspective(60.0, (double)_w / (double)_h, 1.0, 100.0);
}

void idle(void)
{
	// 画面の描き替え
	glutPostRedisplay();
}

void mouse(int _button, int _state, int _x, int _y)
{
	if (_button == GLUT_LEFT_BUTTON){
		if (_state == GLUT_DOWN){
			Trackball.Click(_x, _y);
		}
		else if (_state == GLUT_UP){
			Trackball.Up();
		}
		glutIdleFunc(idle);
		return;
	}
	else	if (_button == GLUT_RIGHT_BUTTON){
		if (_state == GLUT_DOWN){
			cameraTrackball.Click(_x, _y);
		}
		else if (_state == GLUT_UP){
			cameraTrackball.Up();
		}
		glutIdleFunc(idle);
		return;
	}
}

void motion(int _x, int _y)
{
	// トラックボール移動
	Trackball.Drag(_x, _y);
	cameraTrackball.Drag(_x, _y);
	if (cameraTrackball.GetClickFlag()){
		cameraPos[0] = 0, cameraPos[1] = 0, cameraPos[2] = 500;
		cameraTrackball.RotateVec(cameraPos);
		cameraUp[0] = 0; cameraUp[1] = 1; cameraUp[2] = 0;
		cameraTrackball.RotateVec(cameraUp);
	}

	glutPostRedisplay();
}

void keyboard(unsigned char _key, int _x, int _y)
{
	// ESC か q をタイプしたら終了
	if (_key == 'q' || _key == '\033'){
		cout << "おしり";
		exit(0);
	}
	else if (_key == 'R') Trackball.uni();
	else if (_key == 'c') capture_flag = true;


	glutPostRedisplay();
}








