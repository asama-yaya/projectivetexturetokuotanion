#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <Windows.h>
#include <math.h>
#include <time.h>

using std::cout;
using std::endl;

using std::vector;
using std::string;
using std::to_string;
using std::stoi;
using std::stod;
using std::stoul;

using std::ifstream;
using std::ofstream;
using std::ios;

// OpenCV---------------------------------------------------------------------------------------
#include <opencv2/opencv.hpp>		// インクルードファイル指定 
#include <opencv2/opencv_lib.hpp>	// 静的リンクライブラリの指定
// OpenGL --------------------------------------------------------------------------------------
#include <glew.h>
#include <glut.h>
#pragma comment(lib, "glew.lib")
#pragma comment(lib, "glut32.lib")
// MyHeader ------------------------------------------------------------------------------------
#pragma once
#include "AsaVertex.h"
#include "AsaTrackball.h"
#include "OBJLoader.h"

//**********************************************************************************************
// variable
//**********************************************************************************************
int window_width = 640;
int window_height = 480;

TTrackball Trackball;
OBJMESH Objmesh;

// flag
bool capture_flag = false;

// camera
double cameraPos[3] = { 0, 0, 500 };
double cameraUp[3] = { 0, 1, 0 };
TTrackball cameraTrackball;

// shader
GLuint glProgram;
GLuint p_testMatPC, p_testMatNormal, p_testMatM;
GLuint testTexId = -1;

// projective texture
void Projective_init(void);
GLfloat proPosition[3] = { 150, 150, 150 };
GLfloat proTarget[3] = { 0, 0, 0 };
GLfloat proUp[3] = { 0, 1, 0 };

//**********************************************************************************************
// Method
//**********************************************************************************************
// shader
void InitShader();

// 板の描画
void DrawPlanes();

// shader 初期化関連
void LoadGLTexture(string _fileName, GLuint &_texID, const bool _rectFlag = false);
void MakeShader(GLuint &_gl2Program, string _vertName, string _fragName);
int ReadShaderSource(GLuint _shader, const char *_file);
void PrintShaderInfoLog(GLuint _shader);
void PrintProgramInfoLog(GLuint _program);

// glの関数
void display(void);
void resize(int _w, int _h);
void mouse(int _button, int _state, int _x, int _y);
void motion(int _x, int _y);
void keyboard(unsigned char _key, int _x, int _y);
void idle(void);